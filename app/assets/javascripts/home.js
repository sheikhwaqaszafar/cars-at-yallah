cars_data = [];
jQuery(function ($) {
    //search button functionality
    $("#search").click(function (e) {
        $("#dvData").hide();
        $("#cars-details-container").html('');
        var site_name = $('.search-box').val();
        if (/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/.test(site_name)) {
            $(".loading").show();
            $.ajax({
                url: '/cars_at_yallah',
                type: "POST",
                data: {
                    site_url: site_name
                },
                success: function (res) {

                    $(".csv_form input[type='submit']").addClass("button");
                    $('.csv_form').append('<input type="hidden" class="actual_data" name="car_data_to_csv"  />');
                    var last_model = res[res.length-1].Title;
                    for (i = 0; i < res.length; i++) {
                        //cars_data.push([res[i].Type, res[i].Title, ""]);
                        $(".loading").show();
                        $.ajax({
                            url: '/car_versions_at_yallah',
                            type: "POST",
                            data: {
                                site_url: res[i].link,
                                model_name: res[i].Title
                            },
                            success: function (response) {
                                var model_name = response[0].Title;
                                for (j = 0; j < response.length; j++) {
                                    $("#cars-details-container").append("<tr><td class='middle-fit'>" + response[j].Type + "</td><td class='middle-fit'>" + response[j].Title + "</td><td class='middle-fit'>" + response[j].Price + "</td></tr>")
                                    cars_data.push([response[j].Type, response[j].Title, response[j].Price]);
                                }
                                $("#dvData").show();
                                $('.actual_data').val(JSON.stringify(cars_data));
                                if(model_name == last_model)
                                {
                                    $(".loading").hide();
                                }
                            },
                            error: function () {
                            }
                        });
                    }
                },
                error: function () {
                    $(".loading").hide();
                    var message = 'Something went wrong. Please Enter a valid url E.g. http://uae.yallamotor.com/new-cars/toyota ';
                    show_message(message);
                }
            });
        } else {
            var message = 'You have Entered an invalid url. Please Enter a valid url E.g. http://uae.yallamotor.com/new-cars/toyota ';
            show_message(message);
        }
    });
});

//alert bar
var show_message = function (message) {
    $('.alert-box').html('');
    $('.alert-box').append('<p>' + message + '</p>')
    $('.alert-box').show().fadeOut(5000);
}