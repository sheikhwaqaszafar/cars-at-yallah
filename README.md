# README


Things you may want to Know:

* Ruby version 2.3.1

* Rails version 5.0.1

* Mechanize gem has been used for scraping.

* Works on link like http://uae.yallamotor.com/new-cars/toyota, http://uae.yallamotor.com/new-cars/toyota/2016,etc.

* After search hit it will take time to scrap. So be patient, a loader will be shown on your screen. But make sure you have entered a valid url as specified above.

