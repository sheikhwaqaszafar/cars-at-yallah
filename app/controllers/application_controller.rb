class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def scrap_yallah_motors_models(site_url)
    cars_data = []
    mechanize = Mechanize.new
    cars_model_page = mechanize.get(site_url)
    model_name_with_links = cars_model_page.links_with(:class => "new-car-name")
    model_name_with_links.each do |car_model_link|
      cars_data << {:Type => "Car Model", :Title => car_model_link.to_s.strip, :link => "http://uae.yallamotor.com#{car_model_link.uri.to_s}"}
    end
    cars_data
  end

  def scrap_yallah_motors_versions(site_url, model_name)
    cars_data = []
    mechanize = Mechanize.new
    sub_page = mechanize.get(site_url)
    cars_data << {:Type => "Car Model", :Title => model_name, :Price => ""}
    car_versions = sub_page.css(".prices-and-specs .content-box .new-cars-results")
    car_versions.each do |car_version|
      cars_data << {:Type => "Car Version", :Title => car_version.css(".new-cars-results-text h4 a").text.strip, :Price => car_version.css(".new-cars-results-text h4 span").text.strip}
    end
    cars_data
  end

  def generate_csv(data_for_csv)
    csv_file = CSV.generate do |csv|
      csv << ["TYPE", "TITLE", "PRICE"]
      data_for_csv.each do |car_info|
        csv << [car_info[0], car_info[1], car_info[2]]
      end
    end
    csv_file
  end
end
