class HomeController < ApplicationController
  def index
    #Home Page Action
  end

  def cars_at_yallah_motors_models
    site_url = car_models_params[:site_url]
    cars_data = scrap_yallah_motors_models(site_url)
    respond_to do |format|
      format.json { render json: cars_data }
      format.html { render :nothing => true, :status => 200}
    end
  end

  def cars_at_yallah_motors_versions
    site_url = car_versions_params[:site_url]
    model_name = car_versions_params[:model_name]
    cars_data = scrap_yallah_motors_versions(site_url, model_name)
    respond_to do |format|
      format.json { render json: cars_data }
      format.html { render :nothing => true, :status => 200}
    end
  end

  def export_csv
    data_for_csv = JSON.parse(csv_params[:car_data_to_csv])
    csv_file = generate_csv(data_for_csv)
    respond_to do |format|
      format.csv { send_data csv_file, :type => 'text/csv; charset=utf-8; header=present', :disposition => "attachment; filename=CarsAtYallah.csv" }
    end
  end

  def car_models_params
    params.permit(:site_url, :model_name)
  end

  def car_versions_params
    params.permit(:site_url, :model_name)
  end

  def csv_params
    params.permit(:car_data_to_csv)
  end
end




